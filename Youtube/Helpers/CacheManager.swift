//
//  CacheManager.swift
//  Youtube
//
//  Created by Михаил on 07.07.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation

class CacheManager {
    
    static var cache = [String: Data]()
    
    static func setVideoCache(_ url: String, _ data: Data?) {
        
        cache[url] = data
    }

    static func getVideoCache(_ url: String) -> Data? {
        
        return cache[url]
    }
    
}
