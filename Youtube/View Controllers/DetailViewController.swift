//
//  DetailViewController.swift
//  Youtube
//
//  Created by Михаил on 07.07.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var textView: UITextView!
    
    
    //MARK: - Properties
    var video: Video?
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        titleLabel.text = ""
        dateLabel.text = ""
        textView.text = ""
        
        guard video != nil else { return }
        let embedUrlString = Constants.UT_EMBED_URL + video!.videoId
        guard let url = URL(string: embedUrlString) else { return }
        let request = URLRequest(url: url)
        webView.load(request)
        
        titleLabel.text = video?.title
        
        let df = DateFormatter()
        df.dateFormat = "EEEE, MMM d, yyyy"
        self.dateLabel.text = df.string(from: video!.published)
        
        textView.text = video?.description
    }

}
